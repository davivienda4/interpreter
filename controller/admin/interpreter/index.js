const interpreterDb = require('../../../data-access/interpreterDb');

const interpreterSchema = require('../../../validation/schema/interpreter');

const createValidation = require('../../../validation')(interpreterSchema.createSchema);
const updateValidation = require('../../../validation')(interpreterSchema.updateSchema);
const filterValidation = require('../../../validation')(interpreterSchema.filterValidationSchema);
const addInterpreterUsecase = require('../../../use-case/interpreter/addInterpreter')({
  interpreterDb,
  createValidation 
});
const findAllInterpreterUsecase = require('../../../use-case/interpreter/findAllInterpreter')({
  interpreterDb,
  filterValidation
});
const getInterpreterCountUsecase = require('../../../use-case/interpreter/getInterpreterCount')({
  interpreterDb,
  filterValidation
});
const getInterpreterUsecase = require('../../../use-case/interpreter/getInterpreter')({
  interpreterDb,
  filterValidation
});
const updateInterpreterUsecase = require('../../../use-case/interpreter/updateInterpreter')({
  interpreterDb,
  updateValidation 
});
const partialUpdateInterpreterUsecase = require('../../../use-case/interpreter/partialUpdateInterpreter')({ interpreterDb });
const softDeleteInterpreterUsecase = require('../../../use-case/interpreter/softDeleteInterpreter')({ interpreterDb });
const softDeleteManyInterpreterUsecase = require('../../../use-case/interpreter/softDeleteManyInterpreter')({ interpreterDb });
const bulkInsertInterpreterUsecase = require('../../../use-case/interpreter/bulkInsertInterpreter')({ interpreterDb });
const bulkUpdateInterpreterUsecase = require('../../../use-case/interpreter/bulkUpdateInterpreter')({ interpreterDb });
const deleteInterpreterUsecase = require('../../../use-case/interpreter/deleteInterpreter')({ interpreterDb });
const deleteManyInterpreterUsecase = require('../../../use-case/interpreter/deleteManyInterpreter')({ interpreterDb });

const interpreterController = require('./interpreter');

const addInterpreter = interpreterController.addInterpreter(addInterpreterUsecase);
const findAllInterpreter = interpreterController.findAllInterpreter(findAllInterpreterUsecase);
const getInterpreterCount = interpreterController.getInterpreterCount(getInterpreterCountUsecase);
const getInterpreterById = interpreterController.getInterpreter(getInterpreterUsecase);
const updateInterpreter = interpreterController.updateInterpreter(updateInterpreterUsecase);
const partialUpdateInterpreter = interpreterController.partialUpdateInterpreter(partialUpdateInterpreterUsecase);
const softDeleteInterpreter = interpreterController.softDeleteInterpreter(softDeleteInterpreterUsecase);
const softDeleteManyInterpreter = interpreterController.softDeleteManyInterpreter(softDeleteManyInterpreterUsecase);
const bulkInsertInterpreter = interpreterController.bulkInsertInterpreter(bulkInsertInterpreterUsecase);
const bulkUpdateInterpreter = interpreterController.bulkUpdateInterpreter(bulkUpdateInterpreterUsecase);
const deleteInterpreter = interpreterController.deleteInterpreter(deleteInterpreterUsecase);
const deleteManyInterpreter = interpreterController.deleteManyInterpreter(deleteManyInterpreterUsecase);

module.exports = {
  addInterpreter,
  findAllInterpreter,
  getInterpreterCount,
  getInterpreterById,
  updateInterpreter,
  partialUpdateInterpreter,
  softDeleteInterpreter,
  softDeleteManyInterpreter,
  bulkInsertInterpreter,
  bulkUpdateInterpreter,
  deleteInterpreter,
  deleteManyInterpreter,
};