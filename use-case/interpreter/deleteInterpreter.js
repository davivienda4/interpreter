
/**
 *deleteInterpreter.js
 */
 
const response = require('../../utils/response');
/**
 * @description : delete record from database.
 * @param {Object} query : query.
 * @param {Object} req : The req object represents the HTTP request.
 * @param {Object} res : The res object represents HTTP response.
 * @return {Object} : deleted Interpreter. {status, message, data}
 */
const deleteInterpreter = ({ interpreterDb }) => async (query,req,res) => {
  let deletedInterpreter = await interpreterDb.deleteOne(query);
  if (!deletedInterpreter){
    return response.recordNotFound({});
  }
  return response.success({ data: deletedInterpreter });
};

module.exports = deleteInterpreter;