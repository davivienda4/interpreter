/**
 *updateInterpreter.js
 */

const  interpreterEntity = require('../../entities/interpreter');
const response = require('../../utils/response');

/**
 * @description : update record with data by id.
 * @param {Object} params : request body including query and data.
 * @param {Object} req : The req object represents the HTTP request.
 * @param {Object} res : The res object represents HTTP response.
 * @return {Object} : updated Interpreter. {status, message, data}
 */
const updateInterpreter = ({
  interpreterDb, updateValidation
}) => async (params,req,res) => {
  let {
    dataToUpdate, query 
  } = params;
  const validateRequest = await updateValidation(dataToUpdate);
  if (!validateRequest.isValid) {
    return response.validationError({ message : `Invalid values in parameters, ${validateRequest.message}` });
  }
  let interpreter = interpreterEntity(dataToUpdate);
  interpreter = await interpreterDb.updateOne(query,interpreter);
  if (!interpreter){
    return response.recordNotFound();
  }
  return response.success({ data:interpreter });
};
module.exports = updateInterpreter;