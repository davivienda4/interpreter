/**
 *partialUpdateInterpreter.js
 */

const response = require('../../utils/response');

/**
 * @description : partially update record with data by id;
 * @param {Object} params : request body.
 * @param {Object} req : The req object represents the HTTP request.
 * @param {Object} res : The res object represents HTTP response.
 * @return {obj} : updated Interpreter. {status, message, data}
 */
const partialUpdateInterpreter = ({ interpreterDb }) => async (params,req,res) => {
  const interpreter = await interpreterDb.updateOne(params.query,params.dataToUpdate);
  if (!interpreter){
    return response.recordNotFound();
  }
  return response.success({ data:interpreter });
};
module.exports = partialUpdateInterpreter;