/**
 *softDeleteInterpreter.js
 */

const response = require('../../utils/response');

/**
 * @description : soft delete record from database by id;
 * @param {Object} params : request body.
 * @param {Object} req : The req object represents the HTTP request.
 * @param {Object} res : The res object represents HTTP response..
 * @return {Object} : deactivated Interpreter. {status, message, data}
 */
const softDeleteInterpreter = ({ interpreterDb }) => async (params,req,res) => {
  let updatedInterpreter = await interpreterDb.updateOne(params.query, params.dataToUpdate);
  if (!updatedInterpreter){
    return response.recordNotFound();   
  }
  return response.success({ data:updatedInterpreter });
};
module.exports = softDeleteInterpreter;