/**
 *addInterpreter.js
 */

const  interpreterEntity = require('../../entities/interpreter');
const response = require('../../utils/response');
/**
 * @description : create new record of interpreter in database.
 * @param {Object} dataToCreate : data for create new document.
 * @param {Object} req : The req object represents the HTTP request.
 * @param {Object} res : The res object represents HTTP response.
 * @return {Object} : response of create. {status, message, data}
 */
const addInterpreter = ({
  interpreterDb,createValidation 
}) => async (dataToCreate,req,res) => {
  const validateRequest = await createValidation(dataToCreate);
  if (!validateRequest.isValid) {
    return response.validationError({ message : `Invalid values in parameters, ${validateRequest.message}` });
  }
  let interpreter = interpreterEntity(dataToCreate);
  interpreter = await interpreterDb.create(interpreter);
  return response.success({ data:interpreter });
};
module.exports = addInterpreter;