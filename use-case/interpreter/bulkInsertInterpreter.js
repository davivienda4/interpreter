
/**
 *bulkInsertInterpreter.js
 */

const  interpreterEntity = require('../../entities/interpreter');
const response = require('../../utils/response');

/**
 * @description : create multiple records  in database.
 * @param {Object} dataToCreate : data for creating documents.
 * @param {Object} req : The req object represents the HTTP request.
 * @param {Object} res : The res object represents HTTP response.
 * @return {Object} : created Interpreters. {status, message, data}
 */

const bulkInsertInterpreter = ({ interpreterDb }) => async (dataToCreate,req,res) => {
  let interpreterEntities = dataToCreate.map(item => interpreterEntity(item));
  let createdInterpreter = await interpreterDb.create(interpreterEntities);
  return response.success({ data:{ count:createdInterpreter.length || 0 } });
};
module.exports = bulkInsertInterpreter;