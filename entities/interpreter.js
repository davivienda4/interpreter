module.exports = (interpreter) => {

  let newInterpreter = { 
    isDeleted: interpreter.isDeleted,
    isActive: interpreter.isActive,
    createdAt: interpreter.createdAt,
    updatedAt: interpreter.updatedAt,
    addedBy: interpreter.addedBy,
    updatedBy: interpreter.updatedBy,
  };

  // remove undefined values
  Object.keys(newInterpreter).forEach(key => newInterpreter[key] === undefined && delete newInterpreter[key]);

  // To validate Entity uncomment this block
  /*
   * const validate = (newInterpreter) => {
   *   if (!newInterpreter.field) {
   *       throw new Error("this field is required");
   *   }
   * }
   * validate(newInterpreter) 
   */

  return Object.freeze(newInterpreter);
};
