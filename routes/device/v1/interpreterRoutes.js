const express = require('express');
const router = express.Router();
const interpreterController = require('../../../controller/device/v1/interpreter');
const {
  auth,checkRolePermission,
} = require('../../../middleware');
const { PLATFORM } =  require('../../../constants/authConstant'); 

router.route('/device/api/v1/interpreter/create').post(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.addInterpreter);
router.route('/device/api/v1/interpreter/list').post(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.findAllInterpreter);
router.route('/device/api/v1/interpreter/count').post(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.getInterpreterCount);
router.route('/device/api/v1/interpreter/:id').get(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.getInterpreterById);
router.route('/device/api/v1/interpreter/update/:id').put(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.updateInterpreter);   
router.route('/device/api/v1/interpreter/partial-update/:id').put(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.partialUpdateInterpreter);   
router.route('/device/api/v1/interpreter/softDelete/:id').put(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.softDeleteInterpreter);
router.route('/device/api/v1/interpreter/softDeleteMany').put(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.softDeleteManyInterpreter);
router.route('/device/api/v1/interpreter/addBulk').post(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.bulkInsertInterpreter);
router.route('/device/api/v1/interpreter/updateBulk').put(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.bulkUpdateInterpreter); 
router.route('/device/api/v1/interpreter/delete/:id').delete(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.deleteInterpreter);
router.route('/device/api/v1/interpreter/deleteMany').post(auth(PLATFORM.DEVICE),checkRolePermission,interpreterController.deleteManyInterpreter);

module.exports = router;
