const express = require('express');
const router = express.Router();
const interpreterController = require('../../../controller/client/v1/interpreter');
const { auth, } = require('../../../middleware');
const { PLATFORM } =  require('../../../constants/authConstant'); 

router.route('/client/api/v1/interpreter/create').post(interpreterController.addInterpreter);
router.route('/client/api/v1/interpreter/list').post(interpreterController.findAllInterpreter);
router.route('/client/api/v1/interpreter/count').post(interpreterController.getInterpreterCount);
router.route('/client/api/v1/interpreter/:id').get(interpreterController.getInterpreterById);
router.route('/client/api/v1/interpreter/update/:id').put(interpreterController.updateInterpreter);  
router.route('/client/api/v1/interpreter/partial-update/:id').put(interpreterController.partialUpdateInterpreter);  
router.route('/client/api/v1/interpreter/softDelete/:id').put(interpreterController.softDeleteInterpreter);
router.route('/client/api/v1/interpreter/softDeleteMany').put(interpreterController.softDeleteManyInterpreter);
router.route('/client/api/v1/interpreter/addBulk').post(interpreterController.bulkInsertInterpreter);
router.route('/client/api/v1/interpreter/updateBulk').put(interpreterController.bulkUpdateInterpreter);
router.route('/client/api/v1/interpreter/delete/:id').delete(interpreterController.deleteInterpreter);
router.route('/client/api/v1/interpreter/deleteMany').post(interpreterController.deleteManyInterpreter);

module.exports = router;
