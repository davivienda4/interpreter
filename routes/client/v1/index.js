const express =  require('express');
const router =  express.Router();
router.use(require('./interpreterRoutes'));
router.use(require('./userRoutes'));
router.use(require('./uploadRoutes'));

module.exports = router;
