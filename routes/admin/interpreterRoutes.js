const express = require('express');
const router = express.Router();
const interpreterController = require('../../controller/admin/interpreter');
const {
  auth,checkRolePermission,
} = require('../../middleware');
const { PLATFORM } =  require('../../constants/authConstant'); 

router.route('/admin/interpreter/create').post(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.addInterpreter);
router.route('/admin/interpreter/list').post(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.findAllInterpreter);
router.route('/admin/interpreter/count').post(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.getInterpreterCount);
router.route('/admin/interpreter/:id').get(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.getInterpreterById);
router.route('/admin/interpreter/update/:id').put(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.updateInterpreter);   
router.route('/admin/interpreter/partial-update/:id').put(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.partialUpdateInterpreter);   
router.route('/admin/interpreter/softDelete/:id').put(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.softDeleteInterpreter);
router.route('/admin/interpreter/softDeleteMany').put(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.softDeleteManyInterpreter);
router.route('/admin/interpreter/addBulk').post(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.bulkInsertInterpreter);
router.route('/admin/interpreter/updateBulk').put(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.bulkUpdateInterpreter); 
router.route('/admin/interpreter/delete/:id').delete(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.deleteInterpreter);
router.route('/admin/interpreter/deleteMany').post(auth(PLATFORM.ADMIN),checkRolePermission,interpreterController.deleteManyInterpreter);

module.exports = router;
